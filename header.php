<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
    <title><?php echo $title?></title>

    <!-- css -->
    <link href="material/css/base.min.css" rel="stylesheet">
    <link href="material/css/project.min.css" rel="stylesheet">
    <link href="material/css/soon.min.css" rel="stylesheet">

    <!-- favicon -->
    <!-- ... -->
</head>
