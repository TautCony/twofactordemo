<?php $title = "Delete Account"; ?>
<?php require "header.php" ?>

<?php require_once 'GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php'; $website = 'TwoFactorDemo';  ?>
<?php require "databaseConnect.php"; ?>
<body class="page-brand">
<?php require "nav.php" ?>
<main class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-sm-6 col-sm-push-3">
                <section class="content-inner">
                    <div class="card">
                        <div class="card-main">
                            <div class="card-header">
                                <div class="card-inner">
                                    <h1 class="card-heading"><?php echo $title?></h1>
                                </div>
                            </div>
                            <div class="card-inner">
                                <p class="text-center">
                                    <span class="avatar avatar-inline avatar-lg">
                                        <img alt="sign up" src="material/images/users/avatar-001.jpg">
                                    </span>
                                    <?php
                                    if (array_key_exists("name", $_GET)) {
                                        echo "<br/><span>".$_GET["name"]."</span>";
                                    }
                                    ?>
                                </p>
                                <div class="tile-wrap">
                                    <a class="tile waves-attach" href="signup.php">
                                        <div class="tile-inner text-center">
                                            <?php
                                            $authenticator = new PHPGangsta_GoogleAuthenticator();
                                            $user = ""; $code = "";
                                            if (!array_key_exists("name", $_GET) || !array_key_exists("code", $_GET) || !array_key_exists("password", $_GET)) {
                                                echo "Invalid parameter";
                                            } else {
                                                $user = $_GET["name"];
                                                $code = $_GET["code"];
                                                $pass = $_GET["password"];
                                                if ($user == "" || $code == "" || $pass == "") {
                                                    echo "Empty input";
                                                } else {
                                                    if (Verify($user, $pass, $code))
                                                    {
                                                        echo RemoveUser($user)? "SUCCEED": "FAILED";
                                                    }
                                                    else echo "FAILED";
                                                }
                                            }
                                            require "databaseDisconnect.php";
                                            ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
</body>
<?php require "footer.php" ?>
