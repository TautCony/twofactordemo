<?php $title = "HomePage"; $needAvatar = true; ?>
<?php require "header.php" ?>

<body class="page-brand">
<?php require "nav.php"?>
<main class="content">
    <div class="content-header ui-content-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                    <h1 class="content-heading">
                        <?php echo $title?>
                        <div class="margin-no-top pull-right">
                            <div id="plus"    class="soon pull-left" data-face="slide up"></div>
                            <div id="numbers" class="soon pull-left" data-face="slide up"></div>
                            <div id="second"  class="soon pull-left" data-face="slide up"></div>
                        </div>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                <section class="content-inner margin-top-no">
                    <div class="card">
                        <div class="card-main">
                            <div class="card-inner">
                                <p>Click menu to generate a new account or login.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tile tile-collapse">
                        <div data-target="#info_display_tile" data-toggle="tile">
                            <div class="tile-side pull-left">
                                <div class="avatar avatar-sm avatar-brand">
                                    <span class="icon">code</span>
                                </div>
                            </div>
                            <div class="tile-inner">
                                <div class="text-overflow">Welcome to the Google Authenticator demo.</div>
                            </div>
                        </div>
                        <div class="tile-active-show collapse" id="info_display_tile" style="height: 0px;">
                            <div class="tile-sub">
                                <p>
                                    This is a demo for web security class.
                                    <br/><br/><br/>
                                    <span class="pull-right">——Written by TautCony.</span>
                                    <br/>
                                </p>
                            </div>
                            <div class="tile-footer">
                                <div class="tile-footer-btn pull-left">
                                    <a class="btn btn-flat waves-attach waves-effect" data-toggle="tile" href="#info_display_tile"><span class="icon">check</span>&nbsp;OK</a>
                                    <a class="btn btn-flat waves-attach waves-effect" data-backdrop="static" data-toggle="modal" href="#show_group_member"><span class="icon">link</span>&nbsp;Group Member</a>
                                    <a class="btn btn-flat waves-attach waves-effect" href="#"><span class="icon">book</span>&nbsp;PPT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div aria-hidden="true" class="modal modal-va-middle fade" id="show_group_member" role="dialog" tabindex="-1" style="display: none;">
                        <div class="modal-dialog modal-xs">
                            <div class="modal-content">
                                <div class="modal-heading">
                                    <p class="modal-title"><span class="icon margin-right">link</span>Group Member List</p>
                                </div>
                                <div class="modal-inner">
                                    <p class="h5 margin-top-sm text-black-hint">
                                        <?php
                                        require_once "databaseConnect.php";
                                        $list = GetPass("groupList");
                                        $list = explode(" ", $list);
                                        $table = "<table class=\"table\" title=\"Group Member List\"><tbody>\n";
                                        foreach ($list as $member) {
                                            $table = "$table<tr><td>$member</td></tr>\n";
                                        }
                                        $table = "$table</tbody></table>";
                                        echo $table;
                                        require "databaseDisconnect.php";
                                        ?>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <p class="text-right"><a class="btn btn-flat waves-attach waves-effect" data-dismiss="modal">OK</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
</body>
<?php require "footer.php" ?>

<script src="material/js/numbers.js"></script>
<script>
    var m = new Matrix({});
    var plus = new Matrix({});
    var second = new Matrix({});

    plus.initDOM('plus'); plus.setConstant('+');
    second.initDOM('second'); second.setConstant('s');
    m.initDOM('numbers'); m.setValue(0);

    var counter = 0;
    setInterval(function () {
        ++counter;
        m.setValue(counter);
    }, 1000);
</script>

<style>
    .tile-collapse.active {
        margin-right: 0 !important;
        margin-left: 0 !important;
    }
    .tile-collapse {
        margin-bottom: 24px;
    }
</style>
