<header class="header <?php if (isset($needAvatar)) echo "header-transparent header-waterfall"; else echo "header-brand";?> ui-header">
    <ul class="nav nav-list pull-left">
        <li>
            <a data-toggle="menu" href="#ui_menu">
                <span class="icon icon-lg">menu</span>
            </a>
        </li>
    </ul>
    <a class="header-logo margin-left-no" href="#"><?php echo $title?></a>
    <?php if (isset($needAvatar)) require "avatar.php"; ?>
</header>
<nav aria-hidden="true" class="menu" id="ui_menu" tabindex="-1">
    <div class="menu-scroll">
        <div class="menu-content">
            <a class="menu-logo" href="#"><?php echo $title?></a>
            <ul class="nav">
                <li>
                    <a class="collaosed waves-attach" data-toggle="collapse" href="#ui_menu_index">Options</a>
                    <ul class="menu-collapse collapse" id="ui_menu_index">
                        <li><a class="waves-attach" href="index.php" target="_blank"><span class="icon icon-lg margin-right">home</span>HomePage</a></li>
                        <li><a class="waves-attach" href="https://bitbucket.org/TautCony/twofactordemo/src" target="_blank"><span class="icon icon-lg margin-right">code</span>SourceCode</a></li>
                        <li><a class="waves-attach" href="https://github.com/PHPGangsta/GoogleAuthenticator" target="_blank"><span class="icon icon-lg margin-right">link</span>GoogleAuthenticator</a></li>
                        <li><a class="waves-attach" href="http://phpqrcode.sourceforge.net/docs/html/index.html" target="_blank"><span class="icon icon-lg margin-right">link</span>PHP QrCode Library</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>