<?php
function base64_url_encode($input) {
    return strtr(base64_encode($input), '+/=', '-_,');
}

function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_,', '+/='));
}

function GetQrCode($name, $site, $size, $code) {
    $data = urlencode("otpauth://totp/$name?secret=$code&issuer=$site");
    $url  = "https://api.qrserver.com/v1/create-qr-code/?size=$size"."x"."$size&data=$data";
    return "<img src=\"$url\" alt=\"QrCode\"/><br/>";
}

function GetQrCodeLocal($name, $site, $size, $code) {
    $data = urlencode("otpauth://totp/$name?secret=$code&issuer=$site");
    return "<img src=\"qr.php?data=$data\" alt=\"QrCode\"/><br/>";
}