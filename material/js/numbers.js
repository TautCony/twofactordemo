window.Matrix = function() {
	var chars = {
        ' ':[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
	    '0':[[0,1,1,1,0],[1,1,0,1,1],[1,1,0,1,1],[1,1,0,1,1],[1,1,0,1,1],[1,1,0,1,1],[0,1,1,1,0]],
        '1':[[0,0,1,1,0],[0,1,1,1,0],[0,0,1,1,0],[0,0,1,1,0],[0,0,1,1,0],[0,0,1,1,0],[0,1,1,1,1]],
        '2':[[0,1,1,1,0],[1,1,0,1,1],[0,0,0,1,1],[0,0,1,1,0],[0,1,1,0,0],[1,1,0,0,0],[1,1,1,1,1]],
        '3':[[0,1,1,1,0],[1,1,0,1,1],[0,0,0,1,1],[0,0,1,1,0],[0,0,0,1,1],[1,1,0,1,1],[0,1,1,1,0]],
        '4':[[0,0,1,1,1],[0,1,0,1,1],[1,1,0,1,1],[1,1,1,1,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,0,1,1]],
        '5':[[1,1,1,1,1],[1,1,0,0,0],[1,1,0,0,0],[1,1,1,1,0],[0,0,0,1,1],[1,1,0,1,1],[0,1,1,1,0]],
        '6':[[0,1,1,1,0],[1,1,0,0,0],[1,1,1,1,0],[1,1,0,1,1],[1,1,0,1,1],[1,1,0,1,1],[0,1,1,1,0]],
        '7':[[1,1,1,1,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,1,1,0],[0,1,1,0,0],[1,1,0,0,0],[1,1,0,0,0]],
        '8':[[0,1,1,1,0],[1,1,0,1,1],[1,1,0,1,1],[0,1,1,1,0],[1,1,0,1,1],[1,1,0,1,1],[0,1,1,1,0]],
        '9':[[0,1,1,1,0],[1,1,0,1,1],[1,1,0,1,1],[1,1,0,1,1],[0,1,1,1,1],[0,0,0,1,1],[0,1,1,1,0]],
        '+':[[0,0,0,0,0],[0,0,1,0,0],[0,0,1,0,0],[1,1,1,1,1],[0,0,1,0,0],[0,0,1,0,0],[0,0,0,0,0]],
        's':[[0,1,1,1,0],[1,0,0,0,1],[1,0,0,0,0],[0,1,1,1,0],[0,0,0,0,1],[1,0,0,0,1],[0,1,1,1,0]]
	};
	var rowCount = chars[0].length;
	var columnCount = chars[0][0].length;
	var html = "";
	for (var i = 0; rowCount > i; i++) {
		html += '<span class="soon-matrix-row">';
		for (var j = 0; columnCount > j; j++) {
			html += '<span class="soon-matrix-dot"></span>';
		}
		html += "</span>";
	}
	var Numbers = function(ele) {
			this._wrapper = document.createElement("span");
			this._wrapper.className = "soon-matrix " + (ele.className || "");
			this._inner = document.createElement("span");
			this._inner.className = "soon-matrix-inner";
			this._wrapper.appendChild(this._inner);
			this._value = [];
		};
	Numbers.prototype = {
	    initDOM: function (id) {
            document.getElementById(id).appendChild(this.getElement());
        },
		getElement: function() {
			return this._wrapper;
		},
		_addNode: function() {
			var node = document.createElement("span");
			node.className = "soon-matrix-char";
			node.innerHTML = html;
			var ret = { node: node, ref: [] };
			this._updateNode(ret, '0');
			return ret;
		},
		_updateNode: function(node, ch) {
			var matrix = chars[ch];
			var n = node.node.getElementsByClassName("soon-matrix-dot");
			var m = node.ref;
			if (!m.length) {
				for (var i = 0; i < rowCount; i++) {
					m[i] = [];
					for (var j = 0; columnCount > j; j++) {
						m[i][j] = n[i * columnCount + j];
					}
				}
			}
			for (i = 0; rowCount > i; i++) {
				for (j = 0; columnCount > j; j++) {
					m[i][j].setAttribute("data-state", 1 === matrix[i][j] ? "1" : "0");
				}
			}
		},
		setValue: function(num) {
			num += "";
            var offset = Math.max(0, this._value.length - num.length);
            var len = Math.max(num.length, this._value.length);
			for (var i = 0; i < len; ++i) {
			    if (i < offset) {
                    this._updateNode(this._value[i], 0);
			        continue;
                }
				var node = this._value[i];
				if (node === undefined) {
                    node = this._addNode();
                    this._inner.appendChild(node.node);
                    this._value[i] = node;
                }
				this._updateNode(node, num[i-offset]);
			}
		},
        setConstant: function (num) {
            num += "";
            for (var i = 0; i < num.length; ++i) {
                var c = this._addNode();
                this._inner.appendChild(c.node);
                this._updateNode(c, num[i]);
            }
        }
	};
	return Numbers;
}();