<?php $title = "Login"; ?>
<?php require "header.php" ?>

<?php
require "databaseConnect.php";
require_once 'GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php';
$authenticator = new PHPGangsta_GoogleAuthenticator();
?>

<body class="page-brand">
<?php require "nav.php" ?>
<main class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-sm-6 col-sm-push-3">
                <section class="content-inner">
                    <div class="card">
                        <div class="card-main">
                            <div class="card-header">
                                <div class="card-inner">
                                    <h1 class="card-heading"><?php echo $title?></h1>
                                </div>
                            </div>
                            <div class="card-inner">
                                <p class="text-center">
                                    <span class="avatar avatar-inline avatar-lg">
                                        <img alt="Login" src="material/images/users/avatar-001.jpg">
                                    </span>
                                </p>
                                <div class="tile-wrap">
                                    <a class="tile waves-attach" href="index.php">
                                        <div class="tile-inner text-center">
                                            <?php
                                            function Result($message, $url) {
                                                return "<span href='".$url."'>".$message."</span>";
                                            }

                                            $user = ""; $code = ""; $pass = ""; $checkResult = false;
                                            if (!array_key_exists("name", $_GET) || !array_key_exists("code", $_GET)) {
                                                echo "Invalid parameter";
                                                return;
                                            } else {
                                                $user = $_GET["name"];
                                                $code = $_GET["code"];
                                                $pass = $_GET["password"];
                                                if ($user == "" || $code == "" || $pass == "") {
                                                    echo "Empty input";
                                                } else {
                                                    $checkResult = Verify($user, $pass, $code);
                                                    echo "The OTP Validation has ".($checkResult? "SUCCEED": "FAILED");
                                                }
                                            }
                                            ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <?php if ($checkResult) {
                            echo "<p class=\"margin-no-top pull-left\"><a class=\"btn btn-flat btn-brand waves-attach\" href=\"remove.php?name=$user&code=$code&password=$pass\">Remove this account</a></p>";
                            echo "<div id=\"numbers\" class=\"soon margin-no-top pull-right\" data-face=\"slide up\"></div>";
                            } else {
                            echo "<p class=\"margin-no-top pull-right\"><a class=\"btn btn-flat btn-brand waves-attach\" href=\"signup.php\">Create an account</a></p>";
                        }
                        ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
</body>

<?php require "footer.php" ?>
<?php require "databaseDisconnect.php"; ?>

<script src="material/js/numbers.js"></script>
<script>
    var m = new Matrix({});
    m.initDOM('numbers');
    var counter = <?php echo 30 - (date("s") % 30); ?>;
    var id = setInterval(function () {
        --counter;
        if (counter < 0) {
            clearInterval(id);
        }
        m.setValue(counter);
    }, 1000);
</script>
