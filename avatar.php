<ul class="nav nav-list pull-right">
    <li class="dropdown margin-right">
        <a class="dropdown-toggle padding-left-no padding-right-no" data-toggle="dropdown">
            <span class="access-hide">User</span>
            <span class="avatar avatar-sm"><img alt="User" src="material/images/users/avatar-001.jpg"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a class="padding-right-lg waves-attach" href="signup.php">
                    <span class="icon icon-lg margin-right">account_box</span>
                    Sign up
                </a>
            </li>
            <li>
                <a class="padding-right-lg waves-attach" href="login.php">
                    <span class="icon icon-lg margin-right">fingerprint</span>
                    Log in
                </a>
            </li>
        </ul>
    </li>
</ul>