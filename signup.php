<?php $title = "Registration"; ?>
<?php require "header.php" ?>

<body class="page-brand">
<?php require "nav.php" ?>
    <main class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-push-4 col-sm-6 col-sm-push-3">
                    <section class="content-inner">
                        <div class="card">
                            <div class="card-main">
                                <div class="card-header">
                                    <div class="card-inner">
                                        <h1 class="card-heading"><?php echo $title?></h1>
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <p class="text-center">
                                        <span class="avatar avatar-inline avatar-lg">
                                            <img alt="Login" src="material/images/users/avatar-001.jpg">
                                        </span>
                                    </p>
                                    <form class="form" action="generate.php" method="get">
                                        <div class="form-group form-group-label">
                                            <div class="row">
                                                <div class="col-md-10 col-md-push-1">
                                                    <label class="floating-label" for="ui_signup_username">Username</label>
                                                    <input class="form-control" id="ui_signup_username" type="text" required pattern="^[a-zA-Z_].{3,}$" name="name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-label">
                                            <div class="row">
                                                <div class="col-md-10 col-md-push-1">
                                                    <label class="floating-label" for="ui_login_password">Password</label>
                                                    <input class="form-control" id="ui_login_password" type="text" autocomplete="off" required name="password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-10 col-md-push-1">
                                                    <button type="submit" class="btn btn-block btn-brand waves-attach waves-light">Sign Up</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>
</body>
<?php require "footer.php" ?>
