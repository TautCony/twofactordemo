CREATE DATABASE site;
USE site;
CREATE TABLE twofactordemo (id int AUTO_INCREMENT, username VARCHAR(64), secret VARCHAR(64), PRIMARY KEY (id));
ALTER TABLE twofactordemo ADD pass VARCHAR(128);