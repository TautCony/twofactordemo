<?php
require_once 'GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php';
$serverName = "localhost";
$userName   = "root";
$password   = "your password";
$dbName     = "site";

$conn       = new mysqli($serverName, $userName, $password, $dbName);
$auth       = new PHPGangsta_GoogleAuthenticator();

$tableName  = "twofactordemo";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function CheckValid($name) {
    global $conn;
    global $tableName;
    $sql = "SELECT secret FROM $tableName WHERE username = \"$name\"";
    $result = $conn->query($sql);
    if ($result->num_rows == 0) {
        return true;
    }
    return false;
}

function WriteInto($name, $secret, $password) {
    global $conn;
    global $tableName;

    $pass = hash('sha512', $secret . $password);

    $sql = "INSERT INTO ".$tableName." (username, secret, pass) VALUES (\"$name\", \"$secret\", \"$pass\")";
    return $conn->query($sql);
}

function GetSecret($name) {
    global $conn;
    global $tableName;
    $sql = "SELECT secret FROM $tableName WHERE username = \"$name\"";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row["secret"];
    }
    return "";
}

function GetPass($name) {
    global $conn;
    global $tableName;
    $sql = "SELECT pass FROM $tableName WHERE username = \"$name\"";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row["pass"];
    }
    return "";
}

function Verify($name, $givenPassword, $givenCode) {
    $secret = GetSecret($name);
    $pass   = GetPass($name);
    $givenPassword = hash('sha512', $secret . $givenPassword);
    if ($pass != $givenPassword) return false;
    global $auth;
    return $auth->verifyCode($secret, $givenCode, $tolerance = 0);
}

function RemoveUser($name) {
    global $conn;
    global $tableName;
    $sql = "DELETE FROM ".$tableName." WHERE username = \"".$name."\"";
    return $conn->query($sql);
}

/*
 * Create database
CREATE DATABASE site;
USE site;
CREATE TABLE twofactordemo (id int AUTO_INCREMENT, username VARCHAR(64), secret VARCHAR(64), PRIMARY KEY (id));
ALTER TABLE twofactordemo ADD pass VARCHAR(128);
 */
?>

