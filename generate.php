<?php $title = "Registration"; ?>
<?php require "header.php" ?>

<?php require_once 'GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php'; require_once "utils.php";$website = 'TwoFactorDemo'; ?>

<body class="page-brand">
<?php require "nav.php" ?>
<main class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-sm-6 col-sm-push-3">
                <section class="content-inner">
                    <div class="card">
                        <div class="card-main">
                            <div class="card-header">
                                <div class="card-inner">
                                    <h1 class="card-heading"><?php echo $title?></h1>
                                </div>
                            </div>
                            <div class="card-inner">
                                <p class="text-center">
                                    <span class="avatar avatar-inline avatar-lg">
                                        <img alt="sign up" src="material/images/users/avatar-001.jpg">
                                    </span>
                                    <?php
                                    if (array_key_exists("name", $_GET)) {
                                        echo "<br/><span>".$_GET["name"]."</span>";
                                    }
                                    ?>
                                </p>
                                <div class="tile-wrap">
                                    <a class="tile waves-attach" target="_blank" href="signup.php">
                                        <div class="tile-inner text-center">
                                            <?php
                                            $error = false;
                                            if (!array_key_exists("name", $_GET) || !array_key_exists("password", $_GET)) {
                                                echo "Invalid parameter";
                                                $error = true;
                                                return;
                                            }
                                            $user = $_GET["name"];
                                            if (strlen($user) < 4) {
                                                echo "User name too short";
                                                return;
                                            }
                                            $pass = $_GET["password"];
                                            if (strlen($pass) < 6) {
                                                echo "Password too short";
                                                return;
                                            }
                                            include "databaseConnect.php";
                                            if (CheckValid($user)) {
                                                $authenticator = new PHPGangsta_GoogleAuthenticator();
                                                $secret        = $authenticator->createSecret();
                                                //$qrCodeUrl     = $authenticator->getQRCodeGoogleUrl($user, $secret, $website);
                                                //echo "<img src=".$qrCodeUrl." alt=\"QrCode\"></img><br/>";

                                                //$qrCodeUrl     = GetQrCode($user, $website, 200, $secret);
                                                $qrCodeUrl     = GetQrCodeLocal($user, $website, 200, $secret);
                                                echo $qrCodeUrl;
                                                echo "$secret";
                                                WriteInto($user, $secret, $pass);
                                            } else {
                                                echo "The user name: <code>".$user."</code> has existed";
                                                $error = true;
                                            }
                                            include "databaseDisconnect.php";
                                            ?>
                                        </div>
                                    </a>
                                </div>
                                <h2 class="content-sub-heading text-center" style="margin-top: 0;">
                                    <?php echo ($error?"ERROR":"Scan this with<br /> <a href='https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2' target='_blank'>Google Authenticator App</a>"); ?>
                                </h2>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
</body>
<?php require "footer.php" ?>
